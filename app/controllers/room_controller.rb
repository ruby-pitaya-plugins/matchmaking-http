require_relative './controller_base.rb'

module Matchmaking

	class RoomController < ControllerBase

		def authenticate
      room_id = @params[:roomId]

      room = Room.find_by_id(room_id)
      return response_error("Invalid room id") if room.nil?

      @session.uid = room_id
      bind_session_response = @postman.bind_session(@session)

      unless bind_session_response.dig(:error, :code).nil?
        return response = {
          code: RubyPitaya::StatusCodes::CODE_AUTHENTICATION_ERROR,
          msg: 'Error to authenticate',
        }
      end

      return response = {
        code: StatusCodes::CODE_OK,
      }
    end

		def set_room_ready(room_id, has_notification)
      room = Room.find_by_id(room_id)
			room.ready = true
			room.save!

			set_is_room_ready(room_id, true)

      payload = {
        data: room.to_hash(with_user_ids: false),
      }

			if has_notification
				route_room_ready = @setup['matchmaking.route.roomReady']
				room.user_ids.each do |room_user_id|
					@postman.push_to_user(room_user_id, route_room_ready, payload)
				end
			end

      return response = {
        code: StatusCodes::CODE_OK,
      }
    end

		def get_room_data(room_id)
      room = Room.find_by_id(room_id)
		
			match_config = @bll[:matchmaking_data_getter].get_match_config(room)
      player_infos = @bll[:matchmaking_data_getter].get_player_infos(room.user_ids)

      room_data = {
        serverHost: room.host,
        serverPort: room.port,
        gameMode: room.game_mode,
        numberOfPlayers: room.user_ids.size,
        matchConfig: match_config,
        playerInfos: player_infos,
      }

      return response = {
        code: StatusCodes::CODE_OK,
        data: room_data,
      }
		end

		private

		def response_error(message)
      response = {
        code: StatusCodes::CODE_ERROR,
        msg: message,
      }
    end

		def get_is_room_ready_key(room_id)
			return "#{@setup['plugin.matchmaking.redis.keyIsRoomReady']}::#{room_id}"
		end

		def set_is_room_ready(room_id, is_room_ready)
			key_is_room_ready = get_is_room_ready_key(room_id)

			@redis.set(key_is_room_ready, is_room_ready, ex: 120)
		end
	end
end