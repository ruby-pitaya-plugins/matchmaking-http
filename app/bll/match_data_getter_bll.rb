module Matchmaking

  class MatchDataGetterBLL

    def initialize(config)
      @config = config
    end

    def get_match_config(room)
      match_config = {
        maxNumberOfPlayers:  @config['plugin/matchmaking'][:maxPlayers],
        matchDurationSeconds: 20,
      }
    end

    def get_player_infos(user_ids)
      user_ids.map do |user_id|
        player_info = {
          userId: user_id
        }
      end
    end
  end
end
