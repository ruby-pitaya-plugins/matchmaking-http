require 'active_record'

module Matchmaking

  class Room < ActiveRecord::Base

    validates_presence_of :host, :port, :user_ids, :container_id

    def to_hash(with_user_ids: true)
      {
        serverHost: self.host,
        serverPort: self.port,
        gameMode: self.game_mode,
        userIds: with_user_ids ? self.user_ids : []
      }
    end
  end
end