require_relative './controller_base.rb'

module Matchmaking

	class MatchmakingController < ControllerBase

		def join_match(user_id, has_notification)
			game_mode = @params[:gameMode] || @config['plugin/matchmaking'][:defaultGameMode]
			skip_matchmaking = @params[:skipMatchmaking] || false


			if skip_matchmaking == true || skip_matchmaking == 'true'
				Thread.new do
				create_room(game_mode, [user_id], has_notification)
			end
				return response = {
					code: StatusCodes::CODE_OK,
				}
			end

			set_user_found_match(user_id, '', false)

			timeout = @config['plugin/matchmaking'][:waitMatchTimeout]
			min_players = @config['plugin/matchmaking'][:minPlayers]
			max_players = @config['plugin/matchmaking'][:maxPlayers]

			result = redis_eval('join_match', game_mode, user_id, timeout, min_players, max_players)

			start_try_start_match(has_notification)

			return response = {
				code: StatusCodes::CODE_OK,
			}
		end

		def stop_join_match(user_id)
      result = redis_eval('stop_join_match', user_id)

      return response = {
        code: StatusCodes::CODE_OK,
      }
		end

		def has_found_match?(user_id)
			has_found_match = get_user_found_match(user_id)

			room_id = ''
			room_id = get_user_room_id(user_id) if has_found_match

			return response = {
				code: StatusCodes::CODE_OK,
				data: {
					roomId: room_id,
					hasFoundMatch: has_found_match,
				}
			}
		end

		def is_room_ready?(room_id)
			is_room_ready = get_is_room_ready(room_id)

			room_data = {}
			room_data = Matchmaking::Room.find_by_id(room_id).to_hash(with_user_ids: false) if is_room_ready

			return response = {
				code: StatusCodes::CODE_OK,
				data: {
					roomData: room_data,
					isRoomReady: is_room_ready,
				}
			}
		end

		private

		def redis_eval(method_name, *argv)
      timestamp = Time.now.utc.to_i

      argv = [method_name, timestamp] + argv

      @script ||= File.open(Path::SCRIPT_MATCHMAKING_PATH, &:read)
      result = @redis.eval(@script, argv: argv)

      return JSON.parse(result, symbolize_names: true) unless result.nil?
      return nil
    end

		def start_try_start_match(has_notification)
      return if @is_trying_start_match
      @is_trying_start_match = true

      Thread.new do
        loop do
          sleep(0.5)
          results = redis_eval('try_start_match')

          results.each do |result|
            if result[:can_start_match]
							@log.info "Matchmaking | Start Match | result: #{result}"
              user_ids = result[:player_ids]
              game_mode = result[:pool_key]
              create_room(game_mode, user_ids, has_notification)
            else
              if result[:timeout]
								@log.info "Matchmaking | Timeout | result: #{result}"
								user_ids = result[:player_ids]
                notify_match_found_timeout(user_ids) if has_notification
              end
            end
          end
          sleep(1.5)

        rescue Exception => error
          @log.info "ERROR: #{error}"
          @log.info error.backtrace.join("\n")

          @is_trying_start_match = false
          start_try_start_match(has_notification)
        end
      end
    end

		def create_room(game_mode, room_user_ids, has_notification)
			@log.info "Matchmaking | Creating room"

      room = @bll[:matchmaking_room].create_room(game_mode, room_user_ids)

			set_is_room_ready(room.id, false)

      room_user_ids.each do |room_user_id|
				set_user_found_match(room_user_id, room.id, true)
      end

			return unless has_notification

			route_match_found = @setup['plugin.matchmaking.route.matchFound']

			payload = {
        data: room.to_hash,
      }

      room_user_ids.each do |room_user_id|
        @postman.push_to_user(room_user_id, route_match_found, payload)
      end
    end

		def notify_match_found_timeout(user_ids)
			routeMatchFoundTimeout = @setup['plugin.matchmaking.route.matchFoundTimeout']

			user_ids.each do |user_id|
				@postman.push_to_user(user_id, routeMatchFoundTimeout, {})
			end
		end

		def get_has_found_match_key(user_id)
			return "#{@setup['plugin.matchmaking.redis.keyHasFoundMatch']}::#{user_id}"
		end

		def get_user_room_id_key(user_id)
			return "#{@setup['plugin.matchmaking.redis.keyUserRoomId']}::#{user_id}"
		end

		def get_is_room_ready_key(room_id)
			return "#{@setup['plugin.matchmaking.redis.keyIsRoomReady']}::#{room_id}"
		end

		def set_user_found_match(user_id, room_id, has_found_match)
			key_has_found_match = get_has_found_match_key(user_id)
			key_user_room_id = get_user_room_id_key(user_id)

			@redis.set(key_user_room_id, room_id, ex: 120)
			@redis.set(key_has_found_match, has_found_match.to_s, ex: 120)
		end

		def get_user_found_match(user_id)
			key_has_found_match = get_has_found_match_key(user_id)
			has_found_match = @redis.get(key_has_found_match) == 'true'
			return has_found_match
		end

		def get_user_room_id(user_id)
			key_user_room_id = get_user_room_id_key(user_id)
			room_id = @redis.get(key_user_room_id)
			return room_id
		end

		def set_is_room_ready(room_id, is_room_ready)
			key_is_room_ready = get_is_room_ready_key(room_id)

			@redis.set(key_is_room_ready, is_room_ready, ex: 120)
		end

		def get_is_room_ready(room_id)
			key_is_room_ready = get_is_room_ready_key(room_id)
			is_room_ready = @redis.get(key_is_room_ready) == 'true'
			return is_room_ready
		end
	end
end