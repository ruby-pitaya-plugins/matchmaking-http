module Matchmaking

	class RoomHandler < RubyPitaya::HandlerBase

		non_authenticated_actions :authenticate

		def authenticate
			controller = Matchmaking::RoomController.new
			controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

			return controller.authenticate
    end

		def setRoomReady
			room_id = @session.uid
			has_notification = true

			controller = Matchmaking::RoomController.new
			controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

			return controller.set_room_ready(room_id, has_notification)
    end

		def getRoomData
			room_id = @session.uid

			controller = Matchmaking::RoomController.new
			controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

			return controller.get_room_data(room_id)
    end

		private

		def response_error(message)
      response = {
        code: StatusCodes::CODE_ERROR,
        msg: message,
      }
    end
	end
end