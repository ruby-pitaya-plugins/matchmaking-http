require 'active_record'

class CreateRoomMigration < ActiveRecord::Migration[6.1]

  enable_extension 'pgcrypto'

  def change
    create_table :rooms, id: :uuid do |t|
      t.string :container_id, null: false
      t.string :host, null: false
      t.integer :port, null: false
      t.string :game_mode, default: 'defaut'
      t.string :user_ids, array: true, null: false
      t.boolean :ready, default: false
      t.timestamps null: false
    end
  end
end
