require 'erb'
require 'ostruct'

module Matchmaking

  class KubernetesRoomBLL

    KUBERNETES_GAMEPLAY_TEMPLATE_FILE = File.join(__dir__, '../files/pod-gameserver-template.yaml')

    def initialize(redis, setup)
      @redis = redis
      @setup = setup

      @initial_port = @setup['plugin.matchmaking.roomBll.kubernetes.initialPort'].to_i
      @final_port = @setup['plugin.matchmaking.roomBll.kubernetes.finalPort'].to_i
      @container_image = @setup['plugin.matchmaking.roomBll.kubernetes.containerImage']
      @service_host = @setup['plugin.matchmaking.roomBll.kubernetes.serviceHost']
      @game_port = @setup['plugin.matchmaking.roomBll.kubernetes.gameServerPort']

      @current_port = 0
    end

    def create_room(game_mode, user_ids)
      use_next_port

      room = Room.new(container_id: 'empty_id', host: @service_host, port: @current_port, game_mode: game_mode, user_ids: user_ids)
      room.save

      namespace = @setup['plugin.matchmaking.roomBll.kubernetes.namespace']
      room_name_prefix = 'gameserver'
      seconds_to_destroy_gameserver = @setup['plugin.matchmaking.roomBll.kubernetes.secondsToDestroyGameServer']

      template_struct = OpenStruct.new(
        name: "#{room_name_prefix}-#{@current_port}",
				number: @current_port,
        port: @game_port,
        container_image: @container_image,
        room_id: room.id
      )

      template = File.open(KUBERNETES_GAMEPLAY_TEMPLATE_FILE, &:read)
      template_result = ERB.new(template).result(template_struct.instance_eval { binding })

      File.open('/root/temp_template.yaml', 'w') { |f| f.write(template_result) }
      puts "kubectl -n #{namespace} apply -f /root/temp_template.yaml"
      output = `kubectl -n #{namespace} apply -f /root/temp_template.yaml`
      File.delete('/root/temp_template.yaml')

      room_name = "#{room_name_prefix}-#{@current_port}"
      fork { sleep(seconds_to_destroy_gameserver); exec("kubectl -n #{namespace} delete pod #{room_name}") }

      next_room_name = "#{room_name_prefix}-#{get_next_port}"
      puts "kubectl -n #{namespace} delete pod #{next_room_name}"
      fork { exec("kubectl -n #{namespace} delete pod #{next_room_name}") }

      room.container_id = template_struct.name
      room.save

      room
    end

    def destroy_room(room_id)
    end

    private

    def use_next_port
      key_current_port = @setup['plugin.matchmaking.roomBll.kubernetes.keyCurrentPort']
      
      result = @redis.eval(
        """
        local key_current_port = ARGV[1]
        local initial_port = tonumber(ARGV[2])
        local final_port = tonumber(ARGV[3])
        local result = {}

        local current_port = redis.call('GET', key_current_port) or 0
        current_port = tonumber(current_port)
        if current_port == 0 then
          current_port = initial_port
        else
          if current_port < final_port then
            current_port = current_port + 1
          else
            current_port = initial_port
          end
        end

        redis.call('SET', key_current_port, current_port)

        result['current_port'] = current_port

        return cjson.encode(result)
        """,
        argv: [key_current_port, @initial_port, @final_port]
      )
      result = JSON.parse(result).deep_symbolize_keys

      @current_port = result[:current_port]
    end

    def get_next_port
      key_current_port = @setup['plugin.matchmaking.roomBll.kubernetes.keyCurrentPort']

      result = @redis.eval(
        """
        local key_current_port = ARGV[1]
        local initial_port = tonumber(ARGV[2])
        local final_port = tonumber(ARGV[3])
        local result = {}

        local current_port = redis.call('GET', key_current_port) or 0
        current_port = tonumber(current_port)
        if current_port == 0 then
          current_port = initial_port
        end

        local next_port = current_port + 1
        if next_port > final_port then
          next_port = initial_port + 1
        end

        result['next_port'] = next_port

        return cjson.encode(result)
        """,
        argv: [key_current_port, @initial_port, @final_port]
      )
      result = JSON.parse(result).deep_symbolize_keys

      result[:next_port]
    end
  end
end