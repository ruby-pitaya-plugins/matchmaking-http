module Matchmaking

  class ControllerBase

    attr_accessor :bll, :log, :redis, :setup, :config, :params, :session, :postman

    def initialize
      @bll = nil
      @log = nil
      @redis = nil
      @setup = nil
      @config = nil
      @params = nil
      @session = nil
      @postman = nil
    end

    def set_attributes(bll, log, redis, mongo, setup, config, params, session, postman)
      @bll = bll
      @log = log
      @redis = redis
      @mongo = mongo
      @setup = setup
      @config = config
      @params = params
      @session = session
      @postman = postman
    end
  end
end