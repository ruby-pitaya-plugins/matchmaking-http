require 'rubypitaya/core/http_routes'

RubyPitaya::HttpRoutes.class_eval do

	configure do
		set :show_exceptions, false
  end

	post '/plugin/matchmaking/room/setRoomReady' do
		@log = settings.log
		@redis = settings.redis
		@session = nil
		@postman = nil
		controller = Matchmaking::RoomController.new
		controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

		room_id = @params[:roomId]
		has_notification = false
		@params.permit!

		return controller.set_room_ready(room_id, has_notification).to_json
	end

	get '/plugin/matchmaking/room/getRoomData' do
		@log = settings.log
		@redis = settings.redis
		@session = nil
		@postman = nil
		controller = Matchmaking::RoomController.new
		controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

		room_id = @params[:roomId]
		@params.permit!

		return controller.get_room_data(room_id).to_json
	end

  private

  def response_error(message)
    response = {
      code: StatusCodes::CODE_ERROR,
      message: message,
    }.to_json
  end
end