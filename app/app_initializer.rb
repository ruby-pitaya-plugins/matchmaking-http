module Matchmaking
  class AppInitializer < RubyPitaya::InitializerBase
  
    # method:     run
    # parameter:  initializer_content
    # attributes:
    #  - bll
    #    - class: RubyPitaya::InstanceHolder
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/instance_holder.rb
    #    - methods:
    #      - add_instance(key, instance)
    #        - add any instance to any key
    #      - [](key)
    #        - get instance by key
    #  - redis
    #    - link: https://github.com/redis/redis-rb/
    #  - mongo
    #    - class: Mongo::Client
    #    - link: https://docs.mongodb.com/ruby-driver/current/tutorials/quick-start/
    #  - config
    #    - class: RubyPitaya::Config
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/config.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - setup
    #    - class: RubyPitaya::Setup
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/setup.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - log
    #    - class: Logger
    #    - link: https://ruby-doc.org/stdlib-2.6.4/libdoc/logger/rdoc/Logger.html
    #    - methods:
    #      - info
    #        - log information
  
    def run(initializer_content)
      bll = initializer_content.bll
      redis = initializer_content.redis
      setup = initializer_content.setup
      config = initializer_content.config
  
      room_bll = create_room_bll(redis, setup)
      match_data_getter_bll = MatchDataGetterBLL.new(config)

      bll.add_instance(:matchmaking_room, room_bll)
      bll.add_instance(:matchmaking_data_getter, match_data_getter_bll)
    end
  
    def self.path
      __FILE__
    end

    private

    def create_room_bll(redis, setup)
      current_room_bll = setup['plugin.matchmaking.currentRoomBll']
  
      room_bll = NullObjectRoomBLL.new
      if current_room_bll == 'local'
        room_bll = LocalRoomBLL.new(setup)
      elsif current_room_bll == 'docker'
        room_bll = DockerRoomBLL.new(redis, setup)
      elsif current_room_bll == 'kubernetes'
        room_bll = KubernetesRoomBLL.new(redis, setup)
      else
        room_bll = NullObjectRoomBLL.new
      end

      return room_bll
    end
  end
end