require 'rubypitaya/core/http_routes'

RubyPitaya::HttpRoutes.class_eval do

	post '/plugin/matchmaking/joinMatch' do
		@log = settings.log
		@redis = settings.redis
		@session = nil
		@postman = nil
		@controller = Matchmaking::MatchmakingController.new
		@controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

		user_id = @params[:userId]
		has_notification = false
		@params.permit!

		return @controller.join_match(user_id, has_notification).to_json
	end

	get '/plugin/matchmaking/hasFoundMatch' do
		@log = settings.log
		@redis = settings.redis
		@session = nil
		@postman = nil
		@controller = Matchmaking::MatchmakingController.new
		@controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

		user_id = @params[:userId]
		has_notification = false
		@params.permit!

		return @controller.has_found_match?(user_id).to_json
	end

	post '/plugin/matchmaking/stopJoinMatch' do
		@log = settings.log
		@redis = settings.redis
		@session = nil
		@postman = nil
		@controller = Matchmaking::MatchmakingController.new
		@controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

		user_id = @params[:userId]
		has_notification = false
		@params.permit!

		return @controller.stop_join_match(user_id).to_json
	end

	get '/plugin/matchmaking/isRoomReady' do
		@log = settings.log
		@redis = settings.redis
		@session = nil
		@postman = nil
		@controller = Matchmaking::MatchmakingController.new
		@controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

		room_id = @params[:roomId]
		@params.permit!

		return @controller.is_room_ready?(room_id).to_json
	end

  private

  def response_error(message)
    response = {
      code: StatusCodes::CODE_ERROR,
      message: message,
    }.to_json
  end
end