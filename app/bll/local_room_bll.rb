require 'net/http'

module Matchmaking

  class LocalRoomBLL

    def initialize(setup)
      @setup = setup
    end

    def create_room(game_mode, user_ids)
      hostname = @setup['plugin.matchmaking.roomBll.local.hostname']
      game_server_url = @setup['plugin.matchmaking.roomBll.local.gameServerUrl']
      game_server_port = @setup['plugin.matchmaking.roomBll.local.gameServerPort']

      room = Room.new(container_id: 'empty_id',
                      host: hostname,
                      port: game_server_port,
                      game_mode: game_mode,
                      user_ids: user_ids)
      room.save

      send_roomid_to_gameserver(room.id)

      room
    end

    def destroy_room(room_id)
    end

    private

    def send_roomid_to_gameserver(room_id)
      hostname = @setup['plugin.matchmaking.roomBll.local.gameServerUrl']
      hostport = @setup['plugin.matchmaking.roomBll.local.hostport']

      puts
      puts
      puts "http://#{hostname}:#{hostport}?roomId=#{room_id}"
      puts
      puts

      uri = URI("http://#{hostname}:#{hostport}?roomId=#{room_id}")
      Net::HTTP.get(uri)
    end
  end
end
