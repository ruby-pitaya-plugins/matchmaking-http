#!/bin/bash

cmd="$@"

echo "=> Waiting for postgres"

RUBYPITAYA_DATABASE_HOST=${RUBYPITAYA_DATABASE_HOST:-'db'}
RUBYPITAYA_DATABASE_PORT=${RUBYPITAYA_DATABASE_PORT:-5432}

./docker/wait_for.sh $RUBYPITAYA_DATABASE_HOST:$RUBYPITAYA_DATABASE_PORT

echo "=> Postgres is ready"

exec $cmd
