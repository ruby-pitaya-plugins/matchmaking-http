module Matchmaking

	class MatchmakingHandler < RubyPitaya::HandlerBase

		non_authenticated_actions :joinMatch, :stopJoinMatch, :hasFoundMatch
		non_authenticated_actions :authenticate

		def authenticate
			user_id = @params[:userId] || SecureRandom.uuid

			@session.uid = user_id
			bind_session_response = @postman.bind_session(@session)

			unless bind_session_response.dig(:error, :code).nil?
				return response = {
					code: RubyPitaya::StatusCodes::CODE_AUTHENTICATION_ERROR,
					msg: 'Error to authenticate',
				}
			end

			response = {
				code: StatusCodes::CODE_OK,
				userId: user_id,
			}
		end

		def joinMatch
			user_id = @session.uid
			has_notification = true

			controller = Matchmaking::MatchmakingController.new
			controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

			return controller.join_match(user_id, has_notification)
		end

		def stopJoinMatch
			user_id = @session.uid

			controller = Matchmaking::MatchmakingController.new
			controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

			return controller.stop_join_match(user_id)
		end

		def hasFoundMatch
			user_id = @session.uid

			controller = Matchmaking::MatchmakingController.new
			controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

			return controller.has_found_match?(user_id)
		end

		def isRoomReady
			room_id = @params[:roomId]

			@controller = Matchmaking::MatchmakingController.new
			@controller.set_attributes(@bll, @log, @redis, @mongo, @setup, @config, @params, @session, @postman)

			return @controller.is_room_ready?(room_id)
		end
	end
end