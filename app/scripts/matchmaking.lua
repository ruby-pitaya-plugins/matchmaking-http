local function new_key(...)
  local t = {...}
  return table.concat(t, ':')
end

local PlayerPool = {}

function PlayerPool:add(player_id)
  redis.call('RPUSH', self.key_players, player_id)
end

function PlayerPool:remove(player_id)
  redis.call('LREM', self.key_players, 1, player_id)
end

function PlayerPool:delete_self()
  redis.call('DEL', self.key_created_at)
  redis.call('DEL', self.key_timeout)
  redis.call('DEL', self.key_min_players)
  redis.call('DEL', self.key_max_players)
  redis.call('DEL', self.key_players)
  redis.call('SREM', 'all_pool_keys', self.pool_key)
end

function PlayerPool:try_start_match(timestamp)
  local delta_time = timestamp - self.created_at
  local n_players_on_pool = self:number_of_players()
  local has_max_players = n_players_on_pool >= self.max_players
  local has_min_players = n_players_on_pool >= self.min_players and delta_time >= self.timeout
  local can_start_match = has_max_players or has_min_players

  if can_start_match then
    local player_ids = redis.call('LRANGE', self.key_players, 0, self.max_players - 1)
    redis.call('LTRIM', self.key_players, self.max_players, -1)

    if self:number_of_players() <= 0 then
      self:delete_self()
    end

    return {
      can_start_match = true,
      pool_key = self.pool_key,
      player_ids = player_ids,
    }
  else
    local timeout = delta_time > self.timeout

    if timeout then
      local player_ids = redis.call('LRANGE', self.key_players, 0, -1)

      self:delete_self()

      return {
        can_start_match = false,
        timeout = true,
        player_ids = player_ids,
      }
    end

    return {
      can_start_match = false,
      timeout = false,
    }
  end
end

function PlayerPool:number_of_players()
  return tonumber(redis.call('LLEN', self.key_players))
end

function PlayerPool.new(pool_key, timestamp, timeout, min_players, max_players)
  local result = {}

  result.pool_key = pool_key
  result.key_players = new_key(pool_key, 'players')
  result.key_timeout = new_key(pool_key, 'timeout')
  result.key_created_at = new_key(pool_key, 'created_at')
  result.key_min_players = new_key(pool_key, 'min_players')
  result.key_max_players = new_key(pool_key, 'max_players')

  result.created_at = redis.call('GET', result.key_created_at) or timestamp
  result.timeout = redis.call('GET', result.key_timeout) or timeout
  result.min_players = redis.call('GET', result.key_min_players) or min_players
  result.max_players = redis.call('GET', result.key_max_players) or max_players

  result.created_at = tonumber(result.created_at)
  result.timeout = tonumber(result.timeout)
  result.min_players = tonumber(result.min_players)
  result.max_players = tonumber(result.max_players)

  redis.call('SET', result.key_created_at, result.created_at)
  redis.call('SET', result.key_timeout, result.timeout)
  redis.call('SET', result.key_min_players, result.min_players)
  redis.call('SET', result.key_max_players, result.max_players)

  redis.call('SADD', 'all_pool_keys', pool_key)

  return setmetatable(result, { __index = PlayerPool })
end

function PlayerPool.get(pool_key)
  local result = {}

  result.pool_key = pool_key
  result.key_players = new_key(pool_key, 'players')
  result.key_timeout = new_key(pool_key, 'timeout')
  result.key_created_at = new_key(pool_key, 'created_at')
  result.key_min_players = new_key(pool_key, 'min_players')
  result.key_max_players = new_key(pool_key, 'max_players')

  result.created_at = redis.call('GET', result.key_created_at)
  result.timeout = redis.call('GET', result.key_timeout)
  result.min_players = redis.call('GET', result.key_min_players)
  result.max_players = redis.call('GET', result.key_max_players)

  result.created_at = tonumber(result.created_at)
  result.timeout = tonumber(result.timeout)
  result.min_players = tonumber(result.min_players)
  result.max_players = tonumber(result.max_players)

  return setmetatable(result, { __index = PlayerPool })
end

function PlayerPool.exists(pool_key)
  return redis.call('SISMEMBER', 'all_pool_keys', pool_key)
end

local function join_match(player_id, pool_key, timestamp, timeout, min_players, max_players)
  local player_pool = PlayerPool.new(pool_key, timestamp, timeout, min_players, max_players)

  player_pool:add(player_id)

  return {
    n_players_on_pool = player_pool:number_of_players()
  }
end

local function stop_join_match(player_id)
  local pool_keys = redis.call('SMEMBERS', 'all_pool_keys')

  for _, pool_key in ipairs(pool_keys) do
    local player_pool = PlayerPool.get(pool_key)
    player_pool:remove(player_id)

    if player_pool:number_of_players() <= 0 then
      player_pool:delete_self()
    end
  end
end

local function try_start_match(timestamp)
  local pool_keys = redis.call('SMEMBERS', 'all_pool_keys')
  local results = {}

  for i, pool_key in ipairs(pool_keys) do
    local player_pool = PlayerPool.get(pool_key)
    results[i] = player_pool:try_start_match(timestamp)
  end

  return results
end

local function call_join_match()
  local timestamp   = tonumber(ARGV[2])
  local pool_key    = ARGV[3]
  local player_id   = ARGV[4]
  local timeout = tonumber(ARGV[5])
  local min_players = tonumber(ARGV[6])
  local max_players = tonumber(ARGV[7])

  local response = join_match(player_id, pool_key, timestamp, timeout, min_players, max_players)
  return cjson.encode(response)
end

local function call_stop_join_match()
  local timestamp   = tonumber(ARGV[2])
  local player_id   = ARGV[3]

  stop_join_match(player_id)
end

local function call_try_start_match()
  local timestamp   = tonumber(ARGV[2])

  local response = try_start_match(timestamp)
  return cjson.encode(response)
end

local method = ARGV[1]

if method == "join_match" then
  return call_join_match()
elseif method == "stop_join_match" then
  return call_stop_join_match()
elseif method == "try_start_match" then
  return call_try_start_match()
end
