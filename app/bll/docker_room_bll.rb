module Matchmaking

  class DockerRoomBLL

    def initialize(redis, setup)
      @redis = redis
      @setup = setup
      @initial_port = @setup['matchmaking.roomBll.docker.initialPort']
      @final_port = @setup['matchmaking.roomBll.docker.finalPort']
      @container_image = @setup['matchmaking.roomBll.docker.containerImage']
      @container_port = @setup['matchmaking.roomBll.docker.containerPort']

      @current_port = 0
    end

    def create_room(game_mode, user_ids)
      use_next_port

      host = "localhost"

      room = Room.new(container_id: 'empty_id', host: host, port: @current_port, game_mode: game_mode, user_ids: user_ids)
      room.save

      puts "docker run -d -e ROOM_ID=#{room.id} -p #{@current_port}:#{@current_port} #{@container_image}"

      id = `docker run -d -e ROOM_ID=#{room.id} -p #{@current_port}:#{@current_port} #{@container_image}`

      room.container_id = id
      room.save

      room
    end

    def destroy_room(room_id)
      room = Room.find_by_id(room_id)

      `docker rm -f #{container_id}`

      room.save
      room
    end

    private

    def use_next_port
      key_current_port = @setup['matchmaking.roomBll.docker.keyCurrentPort']
      
      result = @redis.eval(
        """
        local key_current_port = ARGV[1]
        local initial_port = tonumber(ARGV[2])
        local final_port = tonumber(ARGV[3])
        local result = {}

        local current_port = redis.call('GET', key_current_port) or 0
        current_port = tonumber(current_port)
        if current_port == 0 then
          current_port = initial_port
        else
          if current_port < final_port then
            current_port = current_port + 1
          else
            current_port = initial_port
          end
        end

        redis.call('SET', key_current_port, current_port)

        result['current_port'] = current_port

        return cjson.encode(result)
        """,
        argv: [key_current_port, @initial_port, @final_port]
      )
      result = JSON.parse(result).deep_symbolize_keys

      @current_port = result[:current_port]
    end
  end
end